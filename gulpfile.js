// Include gulp
var gulp = require('gulp'); 

// Include Our Plugins
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var livereload = require('gulp-livereload');

// Lint Task
gulp.task('lint', function() {
    return gulp.src('js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
        //.pipe(livereload());;
});

// Compile Our Sass
gulp.task('sass', function() {
    return gulp.src('css/scss/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('css'));
        //.pipe(livereload());;
});

// Watch Files For Changes
gulp.task('watch', function() {
    //livereload.listen();
    gulp.watch('js/*.js', ['lint']);
    gulp.watch('css/scss/*.scss', ['sass']);
});

// Default Task
gulp.task('default', ['lint', 'sass', 'watch']);