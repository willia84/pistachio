(function($) {
	$.fn.modal = function() {		
		var $content =  $(this).hide();
		var $modal = $('.modal');
		$modal.append($content);
		$modal.hide();
		$content.show();
		var isClick = false;
		var center = function() {
			var startTop = ($(window).height() - $content.height())/2;
			var startLeft = ($(window).width() -  $content.width())/2;
			$modal.css('top', startTop);
			$modal.css('left', startLeft);
		}
		
		$('.modal-button').on('click', function() {
			center();
			$modal.show();
			isClick = true;
		});
		
		$('.modal-dismiss').on('click', function() {
			$modal.hide();
			isClick = false;
		});
		
		$(window).resize(function() {
			if(isClick) {
				center();
			}
		});
		
	};
	
	
})(jQuery);
