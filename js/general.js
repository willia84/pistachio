(function($) {
	function hex(x) {
		return ("0" + parseInt(x).toString(16)).slice(-2);
	}
		
	$.cssHooks.backgroundColor = {
		
		get: function(elem) {
			var bg;
			if (elem.currentStyle)
				bg = elem.currentStyle.backgroundColor;
			else if (window.getComputedStyle)
				bg = document.defaultView.getComputedStyle(elem,
					null).getPropertyValue("background-color");
			if (bg.search("rgb") == -1)
				return bg;
			else {
				bg = bg.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
				return "#" + hex(bg[1]) + hex(bg[2]) + hex(bg[3]);
			}
		}
	};
	
})(jQuery);