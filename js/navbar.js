(function($) {
	
	/** Positive mean lighter
	 *   Negative mean darker
	 */
	var shadeColor = function(color, percent) {  
		var num = parseInt(color.slice(1),16), amt = Math.round(2.55 * percent), R = (num >> 16) + amt, G = (num >> 8 & 0x00FF) + amt, B = (num & 0x0000FF) + amt;
		return "#" + (0x1000000 + (R<255?R<1?0:R:255)*0x10000 + (G<255?G<1?0:G:255)*0x100 + (B<255?B<1?0:B:255)).toString(16).slice(1);
	};
	
	var createChildDiv =  function() {
		var $childs = $('<ul>').append($(".navbar-body ul").children().clone());
		var $childDiv =  $('<div class="navbar-collapsed">').append($childs).hide();
		var $navbar = $(".navbar").after($childDiv);
		var backgroundColor = shadeColor($navbar.css('background-color'), 25);
		var borderColor = shadeColor($navbar.css('background-color'), 15);
		$childDiv.css('background-color', backgroundColor);
		$('.navbar-collapsed ul li').css('border-color', borderColor);
		$('.navbar-collapsed ul li a').css('color', '#F7FFFF');
		$('.navbar-collapsed ul li:last-child').css('border-radius', '0 0 3px 3px');
		return $childDiv;
	};
	
	var isClick = false;
	var $childDiv = createChildDiv();
	
	$(".navbar-button").on('click', function() {
		if(!isClick) {
			$childDiv.slideDown();
		} else {
			$childDiv.slideUp();
		}
		isClick =  !isClick;
	});
	
})(jQuery);


